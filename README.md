# QR-Code Generator

## Theme

- Icon: https://fontawesome.com/icons/qrcode?f=classic&s=solid
  - ![favicon.png](./src/qr-code-gen.web/wwwroot/favicon.png)
- Favicon Generator: https://realfavicongenerator.net
- Color generator: https://mycolor.space/?hex=%23BFDB4F&sub=1
  - #bfdb4f <div style="background:#bfdb4f; width:40px; height:40px; border-radius:10px"></div>
  - #6bc56c <div style="background:#6bc56c; width:40px; height:40px; border-radius:10px"></div>
  - #05a982 <div style="background:#05a982; width:40px; height:40px; border-radius:10px"></div>
  - #008988 <div style="background:#008988; width:40px; height:40px; border-radius:10px"></div>
  - #0a6879 <div style="background:#0a6879; width:40px; height:40px; border-radius:10px"></div>
  - #2f4858 <div style="background:#2f4858; width:40px; height:40px; border-radius:10px"></div>

## Use of icons

<div>Icons made by <a href="https://www.flaticon.com/authors/roundicons" title="Roundicons">Roundicons</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a> and <a href="https://fontawesome.com/" title="FontAwesome">FontAwesome</a></div>
